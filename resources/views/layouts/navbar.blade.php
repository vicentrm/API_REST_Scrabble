<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#"><img src="/img/logoceed.png" width="50%" height="50%"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="../">Inicio<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../gutenberg">Gutenberg</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../difusion-imprenta">Difusión imprenta</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../trabajos-imprenta">Trabajos imprenta</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../primeros-libros">Primeros libros</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../imprenta-valenciana">Imprenta valenciana</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../scrabble">Scrabble</a>
      </li>
    </ul>
    <form  action="">
      <div class="row">
        <div class="col">
          @if(Auth::check())
          <!--Si tenemos la sesion activa*/-->
          <div class="infoUsuario">           
            <a href="../scrabble/dashboard" class="btn btn-light">Dashboard</a>
            <a href="#"
            onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-light">Logout</a>   
                 
          </div>
          @else
            <a href="../scrabble/login" class="btn btn-light">Login</a>
          @endif
        </div>
      </div>
    </form>

  </div>


</nav>