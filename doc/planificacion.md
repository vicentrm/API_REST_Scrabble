# FASE 1 PFM Vicent Ribes Monzó

# 1. Notas tomadas en la reunión con el cliente
El departamento de castellano del CEED nos ha pedido la creación de una plataforma que conmemore el 550 aniversario del fallecimiento de Johannes Gutenberg.
Los objetivos de la plataforma son variados:

- Disponer de un sitio atractivo en el que tanto los alumnos del Grado en Educación Secundaria (GES) como los del Bachillerato puedan obtener información sobre la imprenta.
- Enriquecer la web del instituto, de manera que los visitantes puedan disponer de más información acerca de lo que se hace en el centro.
- Crear cierta fidelización de usuario, tanto alumnos como visitantes. La idea es que los usuarios puedan recibir información actualizada sobre novedades del centro por lo que resulta necesario que se conecten con cierta asiduidad a la página.

Ante estos objetivos proponen la creación de una web con dos áreas:

- Área de información histórica sobre la imprenta
- Será de acceso libre para todo los usuarios.
- Está enfocada a personas entre 18 y 60 años, estudiantes de Bachillerato o GES a distancia.
- Debe de existir un acceso claro a la página del juego, pero siempre en un segundo plano (lo importante en este área es la imprenta).
- Como información sobre su contenido:
Se compondrá como mínimo de 5 subáreas:
    1. Gutenberg.
    2. Difusión de la imprenta.
    3. Los trabajos en una imprenta del siglo XV.
    4. Los primeros libros en España.
    5. Lugares emblemáticos de imprentas en Valencia.
    6. Área Scrabble.
        - El acceso requerirá autenticación, salvo a la subárea de información sobre el juego, que será abierta (ver puntos posteriores).
        - La aplicación está enfocada a personas entre 10 y 99 años que quieren pasar un rato entretenido sin demasiadas complicaciones.
        - Los usuarios podrán pertenecer al CEED (en la actualidad o en cursos anteriores) o no. En cualquiera de los dos casos, es necesario hacer un registro, es decir la creación de usuarios no es automática.
        - Se compondrá de tres subáreas:
            - Información general del juego: reglas, enlaces a sitios de interés, información de captación de usuarios, información acceso a redes sociales,… El cliente da libertad al programador/diseñador para que incluya todo el contenido que considere adecuado con el objetivo de captar usuarios.
            - Dashboard o panel de información del jugadores. En él tiene que aparecer toda aquella información relativa a jugador: datos de perfil, borrar la cuenta, abandonar partidas, estadísticas e histórico de las partidas, ver notificaciones, jugadores más habituales, etc… así como el acceso a las partidas en juego y a solicitar nuevas partidas (retar a otro jugador).
            - Tablero de juego. Debe incluir todo lo necesario para poder jugar una partida de Scrabble contra otro rival.
        - Tanto en el dashboard como en el tablero deben existir zonas dedicada a información del CEED. Esta información no tiene que ser entendida como publicidad, sino más bien como noticias,   avisos, etc.
        En función de si el usuario es alumno o no del CEED la información mostrada será diferente.
        - Es un juego, por lo que la interactividad ha de ser importante.
        - Por lo demás el cliente se desentiende de la funcionalidad siempre que se c
          consiga crear una aplicación que siga las reglas del juego y sea atractiva para los usuarios.

# 2. Personas:
En este apartado vamos a imaginar a 4 personas los mas reales posibles que puedan llegar a utilizar la nueva web. Este paso lo realizamos para poder acercarnos más al cliente final.

- David tiene ahora 27 años, lleva muchos años estudiando la oposicón de bombero y porfin ahora a aprobado. Le han dado una plaza casi al lado de casa, el vive en Oliva y el parque de bomberos se encuentra en Denia. Le gusta hacer deporte al aire libre como senderismo o escalada, aun asi tambien le gusta tener sus ratitos de vicio en el ordenador.
Un dia que le tocaba guardia estaban el y su compañero aburridos en el parque, en ese momento se puso a “googlear” posibilidades para jugar online al Scarabble y se encontró con la web del CEED y descubrió el juego online que contiene, se lo dijo a todos sus compañeros de trabajo y ahora juegan 1 vs 1 en una liguilla que se han montado, la competición se hace dificil porque todos ellos han estudiado bastante para llegar donde estan y muchas palabras de las que forman son de la jerga de bomberos.

- Vicent, 62 años, en su momento estudio filologia española y ha sido durante mas de 30 años maestro de primaria. Ahora ya jubilado se dedica a cuidar el huerto donde dia a dia va cultivando frutas, hortalizas y verdura, a parte de la dedicación al campo como siempre le ha gustado estudiar esta apuntado a la unversidad de mayores en Gandia donde realizan diversas actividades entretenidas. Por esa dedicación siempre intenta que sus hijos nunca dejen de estudiar y aprender.
Un domingo después de la típica comida de familia su hijo le dice que se ha matriculado en el curso de formación profesional superior de desarrollo de aplicaciones web (DAW) en el CEED, cuando el hijo se fue de casa por curiosidad entro a la web y aprendió algo ese día que a sus 62 años no sabía, la historia de Johannes Gutenberg y la creación de la imprenta, ahora ya puede decir algún dato interesante en la unversidad de mayores.Un domingo después de la típica comida de familia su hijo le dice que se ha matriculado en el curso de formación profesional superior de desarrollo de aplicaciones web (DAW) en el CEED, cuando el hijo se fue de casa por curiosidad entro a la web y aprendió algo ese día que a sus 62 años no sabía, la historia de Johannes Gutenberg y la creación de la imprenta, ahora ya puede decir algún dato interesante en la unversidad de mayores.

- Elena, 30 años, como casi todos sus amigos a los 18 años estudio una carrera, concretamente la carrera de magistero. Aun asi cuando empezo a trabajar se dio cuenta de que no le gustaba su       trabajo, ella siempre habia querido ser chofer de camión y viajar por todo el mundo sobre ruedas.
Acutualmente trabajo exclusiavmente para una gran empresa y esta muy contenta con su trabajo actualmente es chofer de camión y está muy contenta con su trabajo.
Durante los descansos en el trayecto le gusta mucho leer artículos de actualidad informatica sobre todo las relacionadas con la conducción. Ha leído varios artículos y ha visto que su actual empresa está trabajando con camiones autónomos por lo que tiene miedo de perder su trabajo. Como le gusta la informática y ve que en un futuro será una de las profesiones más codiciadas decide formarse en programación en sus ratos libres por lo que empieza a googlear centros de formación, finalmente se topa con la página de CEEDCV y ve que las opiniones del profesorado son muy buenas por lo que decide matricularse, a partir de ese momento lee tanto los artículos sobre la imprenta y sus primeras impresiones y también se mete a jugar alguna que otra partida de scrabble con los compañeros de clase.

- Ana, 15 años, es de una familia muy humilde y de pocos recursos. Con la situación actual de pandemia tanto su madre como su padre se han quedado en paro ya que se dedicaban al sector de la hosteleria. Por supuesto han quitado internet de casa y han tenido que malvender el unico ordenador que tenian.
Aun con la situación de su familia, gracias a la enseñanza gratuita Ana puede seguir estudiando, en clase de historia le han pedido que haga un trabajo sobre la imprenta. Al llegar a casa le pregunta a su padre como podria buscar información y este le dice que la buscara en la enciclopedia que hace muchos años su abuelo le compro a él. Ella piensa que la información de esa enciclopedia puede ser equivoca o obsoleta por lo que decide finalmente dirigirse a la biblioteca del CIPFP Misericórdia. Alli se encuentra casualmente que hay una exposición virtual sobre la imprenta en la web del centro CEEDCV y decide entrar y realizar el trabajo.  Como lo terminó muy pronto vio que podia jugar al scrabble, asi que hecho algunas partidas.

# 3. Modelo conceptual del área Scrabble:

<div align="center">
           <img width="60%" src="img/Modelo%20conceptual.png" alt="Modelo conceptual" title="Modelo conceptual"</img>           
</div>

# 4. Mapa del sitio web:

<div align="center">
           <img width="60%" src="img/mapa_sitio_web.png" alt="Mapa sitio" title="Mapa sitio"</img>           
</div>

# 5. Diagrama de flujo de una partida scrabble:

<div align="center">
           <img width="60%" src="img/diagrama_de_flujo_partida_scrabble.png" alt="Diagrama de flujo" title="Diagrama de flujo"</img>           
</div>
# 6. Modelos alámbricos de áreas y subáreas.

## a. Área de información histórica sobre la imprenta.
- Ordenador
<div align="center">
           <img width="60%" src="img/ordenador_inf_sobre_imprenta.png" alt="Informacion historica sobre la imprenta" title="Informacion historica sobre la imprenta"</img>           
</div>
- Móvil
<div align="center">
           <img width="20%" src="img/movil_inf_sobre_imprenta.png" alt="Informacion historica sobre la imprenta" title="Informacion historica sobre la imprenta"</img>           
</div>
- Tablet
<div align="center">
           <img width="30%" src="img/tabletinf_sobre_imprenta.png" alt="Informacion historica sobre la imprenta" title="Informacion historica sobre la imprentajo"</img>           
</div>


## b. Subárea Gutemberg
- Ordenador
<div align="center">
Ordenador
           <img width="60%" src="img/ordenador_gutemberg.png" alt="Subárea Gutemberg" title="Subárea Gutemberg"</img>           
</div>
- Móvil
<div align="center">
           <img width="20%" src="img/movil_gutemebrg.png" alt="Subárea Gutemberg" title="Subárea Gutemberg"</img>           
</div>
- Tablet
<div align="center">
           <img width="30%" src="img/tablet_gutemberg.png" alt="Subárea Gutemberg" title="Subárea Gutemberg"</img>           
</div>


## c. Subárea difusión de la imprenta.
- Ordenador:
<div align="center">
           <img width="60%" src="img/ordenador_difusion.png" alt="Difusion" title="Difusion"</img>           
</div>
- Móvil
<div align="center">
           <img width="20%" src="img/movil_difusion.png" alt="Difusion" title="Difusion"</img>           
</div>
- Tablet
<div align="center">
           <img width="30%" src="img/tablet_difusion.png" alt="Difusion" title="Difusion"</img>           
</div>

## d. Los trabajos en una imprenta del siglo XV.
- Ordenador
<div align="center">
           <img width="60%" src="img/ordenador_trabajos.png" alt="Trabajos" title="Trabajos"</img>           
</div>
- Móvil
<div align="center">
           <img width="20%" src="img/movil_trabajos.png" alt="Trabajos" title="Trabajos"</img>           
</div>
- Tablet
<div align="center">
           <img width="30%" src="img/tablet_trabajos.png" alt="Trabajos" title="Trabajos"</img>           
</div>

## e. Los primeros libros en España.
- Ordenador
<div align="center">
           <img width="60%" src="img/ordenador_libros.png" alt="primeros libros" title="primeros libros"</img>           
</div>
- Móvil
<div align="center">
           <img width="20%" src="img/movil_libros.png" alt="primeros libros" title="primeros libros"</img>           
</div>
- Tablet
<div align="center">
           <img width="30%" src="img/tablet_libros.png" alt="primeros libros" title="primeros libros"</img>           
</div>

## f. Lugares emblemáticos de imprentas en Valencia.
- Ordenador
<div align="center">
           <img width="60%" src="img/ordenador_lugares.png" alt="lugares emblematicos ord" title="lugares emblematicos ord"</img>           
</div>
- Móvil
<div align="center">
           <img width="20%" src="img/movil_lugares.png" alt="lugares emblematicos mov" title="lugares emblematicos mov"</img>           
</div>
- Tablet
<div align="center">
           <img width="30%" src="img/tablet_lugares.png" alt="lugares emblematicos tab" title="lugares emblematicos tab"</img>           
</div>
## g. Area scrabble
- Ordenador
<div align="center">
           <img width="60%" src="img/ordenador_scrabble.png" alt="area scrablle ord" title="area scrabble ord"</img>           
</div>
- Movil
<div align="center">
           <img width="20%" src="img/movil_scrabble.png" alt="area scrablle tab" title="area scrabble tab"</img>           
</div>
- Tablet
<div align="center">
           <img width="30%" src="img/tablet_scrabble.png" alt="area scrablle tab" title="area scrabble tab"</img>           
</div>

## h. Dashboard
- Ordenador
<div align="center">
           <img width="60%" src="img/ordenador_dashboard.png" alt="Dashboard ord" title="Dashboard ord"</img>           
</div>
- Móvil
<div align="center">
           <img width="20%" src="img/movil_dashboard.png" alt="Dashboard mov" title="Dashboard mov"</img>           
</div>
- Tablet
<div align="center">
           <img width="30%" src="img/tablet_dashboard.png" alt="Dashboard mov" title="Dashboard mov"</img>           
</div>
