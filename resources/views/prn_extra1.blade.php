<!--Inlcuimos la plantilla donde tenemos cargado bootstrap y configudas las secciones de cabecera, contenido y pie-->
@extends('layouts.plantilla',
['title' => '', 'css_files' => ['imprenta'],
'js_files' => ['']])


@section ("cabecera")

@endsection

@section ("contenido")

<div class="container">


    <h1>Difusión de la idea</h1>
    <p class="text-justify">
        La idea de Gutenberg de las letras individuales grabadas en metal se difunde
        por Europa
        Comienza en MAINZ (hacia 1450)
    </p>

    <div class="row">

        <div class="col-xl-4 col-sm-12">
            <div class="list-group listaMapa" id="list-tab" role="tablist">
                <a class="list-group-item list-group-item-action list-group-item-dark" id="list-venecia-list" role="tab">1469 Venecia (Italia)</a>
                <a class="list-group-item list-group-item-action list-group-item-dark" id="list-paris-list" role="tab">1470 París (Francia)</a>
                <a class="list-group-item list-group-item-action list-group-item-dark" id="list-brujas-list" role="tab">1471 Brujas (Holanda)</a>
                <a class="list-group-item list-group-item-action list-group-item-dark" id="list-segovia-list" role="tab">1472 Segovia (España)</a>
                <a class="list-group-item list-group-item-action list-group-item-dark" id="list-valencia-list" role="tab">1474 Valencia (España)</a>
                <a class="list-group-item list-group-item-action list-group-item-dark" id="list-westminster-list" role="tab">1476 Westminster (Gran Bretaña)</a>
            </div>
        </div>

        <div class="col-xl-8 col-sm-12">
            <div class="imagenesMapa">
                <img src="img/11 Europa e imprenta Venecia.png" class="img-fluid imagenMapa" title="Europa imprenta">
                <img src="img/11 Europa e imprenta Paris.png" class="img-fluid imagenMapa" title="Europa imprenta">
                <img src="img/11 Europa e imprenta Brujas.png" class="img-fluid imagenMapa" title="Europa imprenta">
                <img src="img/11 Europa e imprenta Segovia.png" class="img-fluid imagenMapa" title="Europa imprenta">
                <img src="img/11 Europa e imprenta Valencia.png" class="img-fluid imagenMapa" title="Europa imprenta">
                <img src="img/11 Europa e imprenta Westminster.png" class="img-fluid imagenMapa" title="Europa imprenta">
            </div>
        </div>


    </div>
</div>
</div>
@endsection


@section ("pie")

@endsection