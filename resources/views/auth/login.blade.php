@extends('layouts.plantilla',
['title' => 'Registro', 'css_files' => ['scr_login'],
'js_files' => ['test_scr_login']])



@section ("contenido")
<div class="contenedorForm">
    <div class="login">
        <div class="cabeceraLogin">
            <h1>Login</h1>
            <p>¡Logeate y empieza a jugar!</p>
        </div>

        <div class="form">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <!-- por razones educativas está desactivado -->
                <label for="email">Correo</label>
                <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
                <br>
                <label for="password">Contraseña</label>
                <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                <a href="/scrabble/password/reset">¿Contraseña olvidada?</a>
                <button type="submit">Entrar</button>

            </form>
        </div>
    </div>
</div>

<!-- Modal Error-->
<div class="modal" tabindex="-1" id="modalError" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Error inicio de sesión</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@if ($errors->isNotEmpty())
<script type="text/javascript">
    $('#modalError').modal('show')
</script>
@endif




</div>
@endsection

@if (session('status'))
<div class="informacion">
    {{ session('status') }}
</div>
@endif
@section ("pie")

@endsection