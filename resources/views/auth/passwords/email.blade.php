@extends('layouts.plantilla',
['title' => '', 'css_files' => ['scr_login'],
'js_files' => ['test_scr_reset']])


@section ("cabecera")

@endsection

@section ("contenido")
<div class="contenedorForm">
    <div class="login">
        <div class="cabeceraLogin">
            <h1>Recuperación contraseña</h1>
        </div>
        <div class="form">
            <form method="POST" action="{{ route('password.email') }}">
                <label for="email">Correo electrónico</label>
                <input id="email" type="email" name="email" required>
                <button type="submit">
                    Envia enlace para el reinicio de contraseña
                </button>
            </form>
        </div>
    </div>
    <!-- Modal Error-->
    <div class="modal" tabindex="-1" id="modalError" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Error recuperación contraseña</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @if ($errors->has('email'))
                    <p>{{ $errors->first('email') }}</p>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
@if ($errors->isNotEmpty())
<script type="text/javascript">
    $('#modalError').modal('show')
</script>
@endif



@endsection


@section ("pie")

@endsection