<!--Inlcuimos la plantilla donde tenemos cargado bootstrap y configudas las secciones de cabecera, contenido y pie-->
@extends('layouts.plantilla',
['title' => 'Error 401', 'css_files' => ['err_404'],
'js_files' => ['']])


@section ("cabecera")


@endsection

@section ("contenido")
<div class="container">
    <div class="noEncontrada">
        <div class="izq">
            <h1>Ooops!</h1>
            <p>No estas autorizado para ver esta página</p>
            <p>Prueba mejor en estos links:</p>
            <div id="listaLinks">
                <ul class="list-group">
                    <li class="list-group-item">
                        <a href="../">Inicio</a>
                    </li>
                    <li class="list-group-item">
                        <a href="./gutenberg">Gutenberg</a>
                    </li>
                    <li class="list-group-item">
                        <a href="./difusion-imprenta">Difusión imprenta</a>
                    </li>
                    <li class="list-group-item">
                        <a href="./trabajos-imprenta">Trabajos imprenta</a>
                    </li>
                    <li class="list-group-item">
                        <a href="./primeros-libros">Primeros libros</a>
                    </li>
                    <li class="list-group-item">
                        <a href="./imprenta-valenciana">Imprenta valenciana</a>
                    </li>
                    <li class="list-group-item">
                        <a href="./scrabble">Scrabble</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="der">
            <div class="scrabble404">
                <div></div>
                <div></div>
                <div></div>
                <div class="ficha"><span>N<sub>1</sub></span></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div class="ficha"><span>A<sub>1</sub></span></div>
                <div class="ficha"><span>U<sub>1</sub></span></div>
                <div class="ficha"><span>T<sub>1</sub></span></div>
                <div class="ficha"><span>O<sub>1</sub></span></div>
                <div class="ficha"><span>R<sub>1</sub></span></div>
                <div class="ficha"><span>I<sub>1</sub></span></div>
                <div class="ficha"><span>Z<sub>10</sub></span></div>
                <div class="ficha"><span>A<sub>1</sub></span></div>
                <div class="ficha"><span>D<sub>2</sub></span></div>
                <div class="ficha"><span>O<sub>1</sub></span></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
</div>
@endsection


@section ("pie")

@endsection