<!--Inlcuimos la plantilla donde tenemos cargado bootstrap y configudas las secciones de cabecera, contenido y pie-->
@extends('layouts.plantilla',
['title' => '', 'css_files' => ['imprenta'],
'js_files' => ['']])


@section ("cabecera")
@endsection

@section ("contenido")

<div class="container">
    <h1>Información historica sobre la imprenta</h1>
    <p>La imprenta es un método mecánico destinado a reproducir textos e imágenes sobre papel, tela u otros materiales.
        En su forma clásica, consiste en aplicar una tinta, generalmente oleosa, sobre unas piezas metálicas (tipos) para
        transferirla al papel por presión. Aunque comenzó como un método artesanal, su implantación a mediados del siglo
        XV trajo consigo una revolución cultural.</p>

    <div class="row row-cols-xl-5 row-cols-lg-3 row-cols-md-3 row-cols-sm-2 row-cols-1 justify-content-center">
        <div class="col">
            <div class="card h-100 text-center">
                <a href="./gutenberg">
                    <img src="img/01 Gutenberg.jpg" class="rounded-circle card-img-top" alt="Gutenberg"></a>
                <div class="card-body">
                    <h5 class="card-title">Gutenberg</h5>
                    <p class="card-txt">Johannes Gutenberg, inventor de la prensa de imprenta con tipos móviles
                        moderna.</p>
                </div>
                <div class="card-footer text-center ">
                    <a href="./gutenberg" class="btn btn-dark">Leer maś</a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card h-100 text-center">
                <a href="./difusion-imprenta">
                    <img src="img/11 Europa e imprenta.png" class="rounded-circle card-img-top" alt="..."></a>
                <div class="card-body">
                    <h5 class="card-title">Difusión</h5>
                    <p class="card-text">La idea de Gutenberg de las letras individuales grabadas en metal se difunde
                        por
                        Europa</p>
                </div>
                <div class="card-footer text-center ">
                    <a href="./difusion-imprenta" class="btn btn-dark">Leer maś</a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card h-100 text-center">
                <a href="./trabajos-imprenta">
                    <img src="img/08 Trabajadores imprenta 1.png" class="rounded-circle card-img-top" alt="..."></a>
                <div class="card-body">
                    <h5 class="card-title">Trabajos</h5>
                    <p class="card-text">Para la imprenta se necesitaban fundamentalmente 3 personas: el componedor, el
                        entintador y el tirador.</p>
                </div>
                <div class="card-footer text-center ">
                    <a href="./trabajos-imprenta" class="btn btn-dark">Leer maś</a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card h-100 text-center">
                <a href="./primeros-libros">
                    <img src="img/14 valencian bible.jpg" class="rounded-circle card-img-top" alt="..."></a>
                <div class="card-body">
                    <h5 class="card-title">Primeros libros</h5>
                    <p class="card-text">El primer libro impreso en España fue el El sinodal de Aguilafuente en 1472 en
                        Segovia.</p>
                </div>
                <div class="card-footer text-center ">
                    <a href="./primeros-libros" class="btn btn-dark">Leer maś</a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card h-100 text-center">
                <a href="./imprenta-valenciana">
                    <img src="img/15 Valencia.Mercado_Central.jpg" class="rounded-circle card-img-top" alt="..."></a>
                <div class="card-body">
                    <h5 class="card-title">Lugares</h5>
                    <p class="card-text">En Valencia podemos gaudir de lugares emblemáticos que realizaban el
                        procedimiento de
                        Gutemberg</p>
                </div>
                <div class="card-footer text-center ">
                    <a href="./imprenta-valenciana" class="btn btn-dark">Leer maś</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@section ("pie")

@endsection