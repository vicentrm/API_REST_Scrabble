@extends('layouts.plantilla',
['title' => '', 'css_files' => ['imprenta','timeline','cardsCounter'],
'js_files' => ['']])


@section ("cabecera")

@endsection

@section ("contenido")
<div class="container-fuild">

</div>
<div class="container">
    <img src="img/scrabbleImagen.png" class="img-fluid mx-auto d-block" style="margin-bottom: 20px;">
    <div class="row">
        <div class="col-12">
            <p>Llega a CEEDCV el juego de inteligencia y vocabulario más conocido del mundo. El Scrabble es uno de los juegos más adictivos que existen. El objetivo de este juego es formar palabras con el máximo de letras posibles horizontal o verticalmente para así conseguir la mayor cantidad de puntos.
            <div class="text-center">
                <a class="btn btn-success" href="./scrabble/register" role="button">Regístrate para jugar</a>
            </div>

            <hr>
            <div class="row">
                <div class="col-md-12 col-lg-8">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="main-timeline2">
                                <div class="timeline">
                                    <span class="icon fa fa-globe"></span>
                                    <a href="#" class="timeline-content">
                                        <h3 class="title">Reta a tus amigos</h3>
                                        <p class="description">
                                            Con nuestro Scrabble Online puedes retar a tus amigos.
                                        </p>
                                    </a>
                                </div>
                                <div class="timeline">
                                    <span class="icon fa fa-home"></span>
                                    <a href="#" class="timeline-content">
                                        <h3 class="title">Cruza palabras</h3>
                                        <p class="description">
                                            ¡Juega donde quieras al mismo juego de siempre! Que el confinamiento no te lo impida.
                                        </p>
                                    </a>
                                </div>
                                <div class="timeline">
                                    <span class="icon fa fa-search"></span>
                                    <a href="#" class="timeline-content">
                                        <h3 class="title">Busca partidas</h3>
                                        <p class="description">
                                            Con nuestro buscador podrás encontrar partidas contra jugadores de tu nivel.
                                        </p>
                                    </a>
                                </div>
                                <div class="timeline">
                                    <span class="icon fa fa-trophy"></span>
                                    <a href="#" class="timeline-content">
                                        <h3 class="title">Suma puntos</h3>
                                        <p class="description">
                                            Cuando mas jueges y mas ganes mas arriba de la clasificación estarás. ¿A que esperas?
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card-counter primary">
                                <i class="fa fa-users"></i>
                                <span class="count-numbers">{{ $numberUsers }}</span>
                                <span class="count-name">Usuarios registrados</span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="card-counter danger">
                                <i class="fa fa-plug"></i>
                                <span class="count-numbers">{{ $connectedUsers }}</span>
                                <span class="count-name">Usuarios conectados</span>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="card-counter success">
                                <i class="fa fa-gamepad"></i>
                                <span class="count-numbers">{{$playingGames}}</span>
                                <span class="count-name">Partidas en juego</span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div id="tutorial">
                <h3>Como jugar</h3>
                <div class="row">
                    <div class="col-4">
                        <div class="list-group" id="list-tab" role="tablist">
                            <a class="list-group-item list-group-item-action list-group-item-dark active" id="list-video-list" data-toggle="list" href="#list-video" role="tab" aria-controls="video">Video eplicativo</a>
                            <a class="list-group-item list-group-item-action list-group-item-dark" id="list-general-list" data-toggle="list" href="#list-general" role="tab" aria-controls="general">General</a>
                            <a class="list-group-item list-group-item-action list-group-item-dark" id="list-colocacion-list" data-toggle="list" href="#list-colocacion" role="tab" aria-controls="colocacion">Colocación</a>
                            <a class="list-group-item list-group-item-action list-group-item-dark" id="list-palabras-list" data-toggle="list" href="#list-palabras" role="tab" aria-controls="palabras">Palabras</a>
                            <a class="list-group-item list-group-item-action list-group-item-dark" id="list-comodines-list" data-toggle="list" href="#list-comodines" role="tab" aria-controls="comodines">Comodines</a>
                            <a class="list-group-item list-group-item-action list-group-item-dark" id="list-fichas-list" data-toggle="list" href="#list-fichas" role="tab" aria-controls="fichas">Fichas</a>
                            <a class="list-group-item list-group-item-action list-group-item-dark" id="list-despliege-list" data-toggle="list" href="#list-despliege" role="tab" aria-controls="despliege">Despliege</a>
                            <a class="list-group-item list-group-item-action list-group-item-dark" id="list-posicion-list" data-toggle="list" href="#list-posicion" role="tab" aria-controls="posicion">Posición relativa</a>
                        </div>
                    </div>
                    <div class="col-8">
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="list-video" role="tabpanel" aria-labelledby="list-video-list">
                                <div class="row">
                                    <div class="col-10">
                                        <h4>Video de como se juega</h4>
                                        <div class="embed-responsive embed-responsive-16by9">
                                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/uAgsHJJVvq0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade show" id="list-general" role="tabpanel" aria-labelledby="list-general-list">
                                <h4>Información general</h4>
                                <p>El juego consiste en formar palabras de dos o más letras y, colocarlas en el tablero, horizontal o verticalmente, de manera que puedan ser leídas de izquierda a derecha o de arriba hacia abajo. Después de la jugada inicial, cada palabra sobre el tablero debe empalmarse con alguna de las incorporadas.</p>
                                <h4>Objetivos</h4>
                                <p>Ganarle al/los oponentes finalizando con mayor puntaje ya sea por finalización del tiempo o porque no queden más fichas.</h4>
                            </div>
                            <div class="tab-pane fade" id="list-colocacion" role="tabpanel" aria-labelledby="list-colocacion-list">
                                <h4>Colocación de letras</h4>
                                <p>Todas las fichas jugadas en un turno deben colocarse en una línea horizontal o vertical y tienen que formar una palabra completa. Si esas fichas están en contacto con letras de filas o columnas contiguas, también con ellas deben formar palabras completas. El jugador obtiene puntos por todas las nuevas palabras que forme durante su turno.
                                </p>
                            </div>
                            <div class="tab-pane fade" id="list-palabras" role="tabpanel" aria-labelledby="list-palabras-list">
                                <h4>Formación de palabras</h4>
                                <p>Las palabras pueden formarse de las siguientes maneras:</p>
                                <ul>
                                    <li>Añadiendo una o más fichas a una palabra que esté colocada sobre el tablero.</li>
                                    <li>Colocando una palabra que se ccure con otra en el tablero. La nueva palabra debe utilizar una de las letras de la palabra que ya se encuentra en el tablero</li>
                                    <li>Colocando una palabra completa paralelamente a otra que ya se encuentra en el tablero, de modo que las fichas contiguas también forman palabras completas.</li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="list-comodines" role="tabpanel" aria-labelledby="list-comodines-list">
                                <h4>Comodines</h4>
                                <p>El Scrabbble incluye dos fichas en blanco, o comodines, los cuales pueden utilizarse en sustitución de cualquier letra.</p>
                                <p>El valor de los comodines es de cero (0) puntos para calcular el valor de la(s) palabras(s) formada(s).
                                    El comodín no puede reemplazar la K ni la W. Está permitido usar los dos comodines en una misma palabra. El premio de 50 puntos otorgado por utilizar las siete fichas del atril se contabiliza incluso utilizando uno o los dos comodines.

                                    Los comodines pueden reemplazar una letra ya utilizada, aunque exista solo una de ellas en el juego. Por ejemplo, puede colocarse "ZUZAR" usando un comodín y la zeta, o los dos comodines, aunque ya se haya utilizado anteriormente la letra zeta.</p>
                            </div>
                            <div class="tab-pane fade" id="list-fichas" role="tabpanel" aria-labelledby="list-fichas-list">
                                <h4>Fichas: Información general</h4>
                                <p><u>Valor numérico de las letras:</u> El valor de cada letra está indicado con una cifra en la parte inferior de la ficha. La puntuación total obtenida en cada turno se calcula sumando el valor de las letras utilizadas en las palabras que se hayan formado en ese turno, más los puntos obtenidos por haber colocado una o más fichas en casillas con premio.</p>
                                <p><u>Casillas con premio para letras:</u> Al colocar una ficha en una casilla azul celeste (doble tanto de letra), se duplica el valor de dicha letra; y al colocarla en una casilla azul marino (triple tanto de letra), se triplica su valor.</p>
                                <p><u>Casillas con premio para palabras:</u> Al colocar una palabra usando una casilla rosada (doble tanto de palabra), se duplica el valor de dicha palabra; y al colocarla usando una casilla roja (triple tanto de palabra), se triplica su valor.</p>
                                <p><u>Palabras con valor múltiple:</u> Si se forma una palabra que ocupa dos casillas de "doble tanto de palabra", primero se duplica el valor de la palabra y luego se reduplica (es decir, se multiplica por 4 la suma de puntos de sus letras); y si son de "triple tanto de palabra", se triplica y luego se vuelve a triplicar (se multiplica por 9).</p>
                                <p><u>Casilla central:</u> La casilla central (marcada con una estrella) es de color rosado, por lo que duplica la puntuación de la primera palabra formada en el tablero.</p>
                                <p><u>Unicidad de premios:</u> Los premios para las letras y palabras aplican solamente la primera vez que se ocupa la casilla de premio. Posteriormente, las fichas que ocupan esas casillas cuentan simplemente por su puntaje real.</p>
                                <p><u>Palabras simultáneas:</u> Cuando en un mismo turno el jugador forma dos o más palabras nuevas, se suma la puntuación de todas ellas. La(s) letra(s) común(es) entre ellas se cuenta(n) en cada palabra, incluido el premio, si lo tiene(n).</p>
                                <p><u>Premio por ‘Scrabble’:</u> Cuando un jugador coloca las 7 fichas de su atril en un solo turno, ha logrado lo que se llama un Cruza Letra o Pleno, y por ello obtiene 50 puntos extra como premio. Estos puntos se suman a la puntuación que haya obtenido en ese turno, después de duplicar o triplicar la puntuación, cuando corresponda</p>
                                <p><u>Cambio de fichas:</u> Cada jugador en su turno podrá optar por cambiar sus fichas. Aquí podrá cambiar desde una fichas hasta todas si así lo desea. Al presionar en "Cambiar fichas" aparecerá una ventana en la cual se deberá seleccionar las fichas que se desea cambiar. Al cambiar fichas (aunque sea solamente una) se perderá el turno.</p>
                            </div>
                            <div class="tab-pane fade" id="list-despliege" role="tabpanel" aria-labelledby="list-despliege-list">
                                <h4>Despliege de fichas en el tablero</h4>
                                <p>Para colocar las fichas en el tablero se deben aplicar las siguientes normas.<u>El sentido de colocación de una palabra está dado por:</u></p>
                                <ul>
                                    <li>La primera ficha colocada en el tablero define la fila o columna en que se colocará la palabra.</li>
                                    <li>La segunda ficha colocada define si la palabra ocupará una fila o una columna, la cual será de uso obligatorio para la jugada completa.</li>
                                    <li>También es posible colocar varias fichas simultáneamente, siempre que se cumpla con los puntos aquí expuestos.</li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="list-posicion" role="tabpanel" aria-labelledby="list-posicion-list">
                                <h4>Posición Relativa</h4>
                                <p>Cuando el jugador en turno termina de colocar en el tablero las fichas de una palabra, <u>solo puede modificar la posición de las letras de esa palabra en los casos siguientes:</u></p>
                                <ul>
                                    <li>La palabra completa puede ser desplazada en la misma fila o columna para que enlace correctamente con la(s) ya existente(s) en el tablero.</li>
                                    <li>La posición relativa de las letras puede cambiarse.</li>
                                    <li>En el caso de que la jugada consista de una sola ficha, ésta solamente podrá ser desplazada a lo largo de la fila y columna donde esté colocada hasta que enlace correctamente con algunas de la(s) palabra(s) ya existente(s) en el tablero.</li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

</div>

@endsection


@section ("pie")

@endsection