<!-- el fichero css de registro me vale para el reset -->
@extends('layouts.plantilla',
['title' => 'Reinicio contraseña', 'css_files' => ['scr_login'],
'js_files' => ['test_scr_reset']])

@section ("cabecera")

@endsection

@section ("contenido")

<div class="contenedorForm">
    <div class="login">
        <div class="cabeceraLogin">
            <h1>Recuperación contraseña</h1>
        </div>
        <div class="form">
            <form method="POST" action="{{ route('password.request') }}">
                @csrf
                <!-- por razones educativas está desactivado -->
                <input type="hidden" name="token" value="{{ $token }}">
                <label for="email">Correo electrónico</label>
                <input id="email" type="email" name="email" value="{{ $email ?? old('email') }}" required autofocus>
                @if ($errors->has('email'))
                <div class="smallError">
                    {{ $errors->first('email') }}
                </div>
                @endif
                <label for="password">Contraseña</label>
                <input id="password" type="password" name="password" required>
                <label for="password-confirm">Confirma contraseña</label>
                <input id="password-confirm" type="password" name="password_confirmation" required>
                @if ($errors->has('password'))
                <div class="smallError">
                    {{ $errors->first('password') }}
                </div>
                @endif
                <button type="submit">
                    Reinicia contraseña
                </button>
            </form>
        </div>
    </div>
</div>
@endsection
@section ("pie")

@endsection