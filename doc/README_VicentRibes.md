# Herramientas utilizadas
- S.O: Kubuntu
- Editor de codigo: Visual Studio Code
- Gestor de git Grafico: GitKraken

# Webs de consulta para codigo web:
- https://www.w3schools.com/
- https://developer.mozilla.org/es/docs/Web/CSS/column-count
- http://codepen.io/
- 

# Framework y utilidades utilizadas:
- BootStrap: https://getbootstrap.com/
- FontAwesome para iconos: https://fontawesome.com/


# Componentes vue externos utilizados:
- Vue highcharts, para graficos de estádisticas: https://github.com/highcharts/highcharts-vue
- UploadImageComponente para el cambio de avatar hecho por Alfredo Oltra.



