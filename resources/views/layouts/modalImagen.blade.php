<div id="modalImagen" class="modal" role="dialog">
    <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <img src="" class="modal-content recibir-imagen" width="100%" height="100%">
        <div class="modal-footer">
            <p><strong class="texto-imagen"></strong></p>
        </div>
    </div>
</div>