<!--Inlcuimos la plantilla donde tenemos cargado bootstrap y configudas las secciones de cabecera, contenido y pie-->
@extends('layouts.plantilla',
['title' => '', 'css_files' => ['imprenta'],
'js_files' => ['']])


@section ("cabecera")

@endsection

@section ("contenido")

<div class="container">
    <h1>Gutenberg</h1>
    <div class="gutenberg">
        <img class="imgIzq" src="img/01 Gutenberg.jpg">
    </div>
    <p class="text-justify"><b>Gutenberg nació en Maguncia, Alemania alrededor de 1400</b> en la casa paterna llamada zum Gutenberg. Su apellido verdadero es Gensfleisch (en dialecto alemán renano este
        apellido tiene semejanza, si es que no significa, «carne de ganso», por lo que el inventor de la
        imprenta en Occidente prefirió usar el apellido por el cual es conocido). Hijo del comerciante
        Federico Gensfleisch, que adoptaría posteriormente hacia 1410 el apellido zum Gutenberg, y de Else Wyrich, hija de un
        tendero.
    </p>
    <p class="text-justify">
        Conocedor del arte de la fundición del oro, se destacó como herrero para el obispado de su ciudad. La familia se trasladó a Eltville am Rhein, ahora en el Estado de Hesse, donde Else
        había heredado una finca. Debió haber estudiado en la Universidad de Erfurt, en donde está registrado en 1419 el
        nombre de Johannes de Alta Villa (Eltvilla). Ese año murió su padre. Nada más se conoce de Gutenberg hasta que en 1434 residió como platero en Estrasburgo,
        donde cinco años después se vio envuelto en un proceso, que demuestra de forma indudable, que Gutenberg había formado una sociedad con Hanz Riffe
        para desarrollar ciertos procedimientos secretos. En 1438 entraron como asociados Andrés Heilman y Andreas Dritzehen (sus herederos fueron los
        reclamantes) y en el expediente judicial se mencionan los términos de prensa, formas e impresión.
    </p>
    <p class="text-justify">
        De regreso a Maguncia, formó una nueva sociedad con Johann Fust, quien le da un préstamo con el que,<b> en 1449, publicó el Misal de Constanza, primer libro
            tipográfico del mundo occidental</b>. Recientes publicaciones, en cambio, aseguran que este misal no pudo imprimirse antes de 1473 debido a la confección de su
        papel, por lo que no debió ser obra de Gutenberg. En 1452, Gutenberg da comienzo a la edición de la Biblia de 42 líneas (también conocida como Biblia de
        Gutenberg). En 1455, Gutenberg carecía de solvencia económica para devolver el préstamo que le había concedido Fust, por lo que se disolvió la unión y
        Gutenberg se vio en la penuria (incluso tuvo que difundir el secreto de montar imprentas para poder subsistir).Johannes Gutenberg murió arruinado en Maguncia, Alemania el 3 de febrero de
        1468. A pesar de la oscuridad de sus últimos años de vida, siempre será reconocido como el inventor de la imprenta moderna.
    </p>
    <div class="text-right">
        <a href="https://es.wikipedia.org/wiki/Johannes_Gutenberg">Fuente wikipedia</a>
    </div>
    <h3>¿Qué es lo que invento?</h3>
    <p class="text-justify">
        El nombre de Gutenberg lo asociamos a la invención de la imprenta, pero
        mucho antes que él ya se imprimía sobre pergamino o papel.
    </p>
    <div class="row">
        <div class="col-4">
            <div class="list-group" id="list-tab" role="tablist">
                <a class="list-group-item list-group-item-action list-group-item-dark active" id="list-list2000-list" data-toggle="list" href="#list-2000" role="tab" aria-controls="list2000">2000 a.Gutenberg</a>
                <a class="list-group-item list-group-item-action list-group-item-dark" id="list-list1400-list" data-toggle="list" href="#list-1400" role="tab" aria-controls="list1400">1400 a.Gutenberg</a>
                <a class="list-group-item list-group-item-action list-group-item-dark" id="list-gutenberg-list" data-toggle="list" href="#list-Gutenberg" role="tab" aria-controls="listGutenberg">Gutenberg</a>
                <a class="list-group-item list-group-item-action list-group-item-dark" id="list-IdeaEficaz-list" data-toggle="list" href="#list-ideaEficaz" role="tab" aria-controls="listideaEficaz">Gutenberg y su triumfo</a>
            </div>
        </div>

        <div class="col-8">
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="list-2000" role="tabpanel" aria-labelledby="list-list2000-list">En Roma se imprimían carteles con signos grabados en arcilla</div>
                <div class="tab-pane fade" id="list-1400" role="tabpanel" aria-labelledby="list-list1400-list">
                    <p>En China se imprimían carteles utilizando signos grabados en madera. Así en el año 686 se imprimió una publicación que se llamó “El sutra del diamante”, con signos grabados en una única madera.</p>
                    <div class="row">
                        <div class="col-6 text-center">
                            <img src="img/02 Sutra del diamante .png" class="imagen" title="Sutra del diamante">
                        </div>
                        <div class="col-6 text-center">
                            <img src="img/03 Tipos chinos en madera.jpg" class="imagen" title="Tipos chinos de madera">
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="list-Gutenberg" role="tabpanel" aria-labelledby="list-gutenberg-list">
                    <p>Los moldes de madera tenían un problema: pronto se desgastaban y se
                        echaban a perder. La gran idea de Gutenberg fue moldear piezas de metal para
                        cada letra, creando de tipos de letras de metal individuales para la imprenta.</p>

                    <div class="row">
                        <div class="col-6 text-center">
                            <img src="img/04 Fundidor de tipos de Gutenberg.png" class="imagen" id="fundidor" title="Fundidor de tipos de Gutenberg">
                        </div>
                        <div class="col-6 text-center">
                            <img src="img/05 tipos moviles metal gutenberg.jpeg" class="imagen" id="tiposMov" title="Tipos moviles metal gutenberg">
                        </div>
                    </div>
                    <p>Después sólo quedaba componer en una cajita, letra a letra, el texto que se
                        quería imprimir, cajita que se manchaba con unos tampones entintados.</p>
                    <div class="d-flex justify-content-center">
                        <img src="img/06 tipos moviles metal gutenberg.jpg" class="imagen" id="tiposMov2" title="Tipos moviles metal gutenberg">
                    </div>
                    <p>Finalmente, sobre las letras metálicas entintadas se colocaba el papel y se
                        presionaba con un aparato así:</p>
                    <div class="d-flex justify-content-center">
                        <img src="img/07 Prensa_de_Gutenberg Replica.png" class="imagen" title="Replica de prensa de Gutenberg">
                    </div>
                </div>
                <div class="tab-pane fade" id="list-ideaEficaz" role="tabpanel" aria-labelledby="list-ideaEficaz-list">
                    <p>En resumen, Gutenberg confeccionó un abecedario con letras y signos de plomo. Su idea fue eficaz porque la prefeccionó con:</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Letras móviles</li>
                        <li class="list-group-item">Molde de metal</li>
                        <li class="list-group-item">Aleación especial de metales para fabricar los móviles (plomo, antimonioy bismuto)</li>
                        <li class="list-group-item">Prensa de madera anclada al suelo y al techo</li>
                        <li class="list-group-item">Tinta de imprimir en un determinado papel</li>
                    </ul>
                </div>
            </div>

        </div>
    </div>

    @endsection


    @section ("pie")

    @endsection