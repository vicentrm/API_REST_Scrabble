<!--Inlcuimos la plantilla donde tenemos cargado bootstrap y configudas las secciones de cabecera, contenido y pie-->
@extends('layouts.plantilla',
['title' => '', 'css_files' => ['imprenta'],
'js_files' => ['']])


@section ("cabecera")

@endsection

@section ("contenido")

<div class="container">

    <h1>Lugares emblemáticos de la imprenta valenciana<h2>
            <!-- Grid row -->
            <div class="gallery" id="gallery">


                <div class="mb-3 pic">
                    <img class="img-fluid" src="img/15 Valencia.Mercado_Central.jpg" alt="Card image cap1">
                    <div class="info">
                        <h3>El molí de Rovella</h3>
                        <p>En la confluencia de las actuales calles Barón de Cárcer y Pie de la Cruz
                            estuvo ubicada la primera imprenta en València.
                        </p>
                    </div>
                </div>

                <div class="mb-3 pic">
                    <img class="img-fluid" src="img/16 El Puig Monasterio.jpg" alt="Card image cap2">
                    <div class="info">
                        <h3>Monasterio de Santa María del Puig</h3>
                        <p>Alberga el Museo de la Imprenta y contiene una réplica exacta de la
                            imprenta utilizada Gutenberg y que se conserva en Maguncia (Alemania).
                        </p>
                    </div>
                </div>

                <div class="mb-3 pic">
                    <img class="img-fluid" src="img/17 portal valldigna 1.jpg" alt="Card image cap3">
                    <div class="info">
                        <h3>Imprenta de Palmart</h3>
                        <p>Junto al Portal de la Valladigna se situaron los talleres de imprenta de
                            donde salieron: “Trobes en loors de la Verge María” “Comprehensorium”
                            “Biblia valenciana” .
                        </p>
                    </div>
                </div>

                <div class="mb-3 pic">
                    <img class="img-fluid" src="img/17 portal valldigna 2.jpg" alt="Card image cap4">
                    <div class="info">
                        <h3>Imprenta de Palmart</h3>
                        <p>Junto al Portal de la Valladigna se situaron los talleres de imprenta de
                            donde salieron: “Trobes en loors de la Verge María” “Comprehensorium”
                            “Biblia valenciana” .
                        </p>
                    </div>
                </div>
                <div class="mb-3 pic">
                    <img class="img-fluid" src="img/19b calle san vicente Patricio May.png" alt="Card image cap6">
                    <div class="info">
                        <h3>Imprenta de Patricio Mey</h3>
                        <p>En el número 3 de la calle San Vicente se imprimió la segunda edición de
                            “Don Quijote de la Mancha”.
                        </p>
                    </div>
                </div>

                <div class="mb-3 pic">
                    <img class="img-fluid" src="img/19 calle san vicente patricio mey 2.jpg" alt="Card image cap5">
                    <div class="info">
                        <h3>Imprenta de Patricio Mey</h3>
                        <p>En el número 3 de la calle San Vicente se imprimió la segunda edición de
                            “Don Quijote de la Mancha”.
                        </p>
                    </div>
                </div>

            </div>
            <!-- Grid row -->

</div>


@endsection