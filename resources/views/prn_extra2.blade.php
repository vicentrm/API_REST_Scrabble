<!--Inlcuimos la plantilla donde tenemos cargado bootstrap y configudas las secciones de cabecera, contenido y pie-->
@extends('layouts.plantilla',
['title' => '', 'css_files' => ['imprenta'],
'js_files' => ['']])

@section ("cabecera")

@endsection

@section ("contenido")

<div class="container">
    <div class="card-group">
        <div class="card">
            <img src="img/08b Trabajadores imprenta 1.png" class="card-img-top imagen" id="componedor" alt="componedor" title="El componedor">

            <div class="card-body">
                <h5 class="card-title">El componedor</h5>
                <p class="card-text">Realiza el trabajo más delicado. A medida que lee el manuscrito coloca en
                    una cajita, una a una, todas las piezas de metal con letras y espacios que
                    forman una línea. Debe hacerlo en orden inverso. Y cajita a cajita,
                    confecciona toda una página.
                </p>
            </div>
        </div>
        <div class="card">
            <img src="img/09b Trabajadores imprenta 2.jpg" class="card-img-top imagen" id="entintador" alt="entintador" title="El entintador">
            <div class="card-body">
                <h5 class="card-title">El entintador</h5>
                <p class="card-text">Encargado de entintar la superficie de letras que ha elaborado el
                    componedor. Par ello utiliza dos tampones semiesféricos impregnados de
                    tinta, uno en cada mano.
                </p>
            </div>
        </div>
        <div class="card">
            <img src="img/10b Trabajadores imprenta 3.jpg" class="card-img-top imagen" id="tirador" alt="tirdaor" title="El tirador">
            <div class="card-body">
                <h5 class="card-title">El tirador</h5>
                <p class="card-text">Coloca papel sobre la superficie de letras entintadas y acciona la palanca
                    que hace bajar la prensa sobre los tipos metálicos que colocó el
                    componedor, de manera que quedan marcadas en el papel.
                </p>
            </div>
        </div>
    </div>
    <hr class="lineaSeparadora">
    </hr>
    <div class="row">
        <div class="col-12">
            <h4>Proceso de impresión con prensa</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <!-- hacemos la incrustiacion del video mediante la clase embed-responsive y le damos una relacion de aspecto 16:9
            https://getbootstrap.com/docs/4.4/utilities/embed/-->
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/M2SanMKYdKk" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>

    </div>
</div>

@endsection


@section ("pie")

@endsection