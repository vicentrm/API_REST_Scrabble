<!doctype html>
<html lang="{{ app()->getLocale() }}">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    <title>{{ config('app.name', 'Laravel') . '. '}} {{ $title or '' }}</title>
    <!-- CSRF Token para evitar ataques de petición de sitios cruzados -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <!-- Css personal 
    <link rel="stylesheet" href="{{ asset('css/imprenta.css') }}">-->

    <!-- Fuente de iconos Font Awesome -->
    <link rel="stylesheet" href="{{ asset('fonts/all.css') }}">

    <!-- Estilos especificos -->
<link rel="stylesheet" href="{{ asset('css/plantilla.css') }}">

    @foreach ($css_files as $file)
    @if($file<>'')
        <link href="{{ asset('css/' . $file . '.css') }}" rel="stylesheet">
        @endif
        @endforeach

</head>

<body>
    @include("layouts.navbar")
    @yield("cabecera")
    @include("layouts.modalImagen")

    <div class="contenido">
        <div id="app">
            @yield('contenido')
        </div>
    </div>

    <!--Bootstrap js-->
    <script src="{{ asset('js/app.js') }}"></script>
    @foreach ($js_files as $file)
    @if($file<>'')
        <script src="{{ asset('js/' . $file . '.js') }}"></script>
        @endif
        @endforeach
        <!--js personal-->
        <script type="text/javascript">
            /*jquery que nos permite para aquellas imagenes de la clase imgaen abrir un modal segun en que imagen hayas hecho click*/
            $(function() {
                $('.imagen').click(function() {
                    var imagen1 = $(this).attr('src');
                    var titleimagen = $(this).attr('title');
                    $('.recibir-imagen').attr('src', imagen1);
                    $('.texto-imagen').text(titleimagen);
                    $('#modalImagen').modal();
                });
            });
        </script>

</body>


</html>