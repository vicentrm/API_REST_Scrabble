<!--Inlcuimos la plantilla donde tenemos cargado bootstrap y configudas las secciones de cabecera, contenido y pie-->
@extends('layouts.plantilla',
['title' => '', 'css_files' => ['imprenta'],
'js_files' => ['']])

@section ("cabecera")

@endsection

@section ("contenido")

<div class="container">

    <h2>Los primeros libros en España </h2>
    <p>El primer libro impreso en España fue el El sinodal de Aguilafuente en 1472 en
        Segovia.</p>
    <p>Por su parte, los tres primeros impresos en València con el procedimiento de
        Gutenberg fueron:
    </p>
    <div class="row">
        <div class="col-md-4">
            <figure class="figure">
                <div class="contenedorImagen">
                    <img src="img/12 les trobes.jpg" class="figure-img img-fluid rounded" alt="Obres o trobes en laros de la Verge Maria-1474" title="Obres o trobes en laors de la Verge Maria - 1474">
                </div>
                <figcaption class="figure-caption">Obres o trobes en laors de la Verge Maria - 1474</figcaption>
            </figure>
        </div>
        <div class="col-md-4">
            <figure class="figure">
                <div class="contenedorImagen">
                    <img src="img/13 el comprensorium.jpg" class="figure-img img-fluid rounded" alt="Comprehensorium-1475" title="Comprehensorium-1475">
                </div>
                <figcaption class="figure-caption">Comprehensorium - 1475</figcaption>
            </figure>
        </div>
        <div class="col-md-4">
            <figure class="figure">
                <div class="contenedorImagen">
                    <img src="img/14 valencian bible.jpg" class="figure-img img-fluid rounded" alt="Biblia valenciana-1478" title="Biblia valenciana-1478">
                </div>
                <figcaption class="figure-caption">Biblia valenciana - 1478</figcaption>
            </figure>
        </div>
    </div>

</div>

@endsection


@section ("pie")

@endsection